import {NgModule} from '@angular/core';
import {RouterModule, Routes} from '@angular/router';
import {BlogComponent} from './blog.component';
import {BlogPostComponent} from './views/blog-post/blog-post.component';
import {CommonModule} from '@angular/common';
import {BlogPostCardComponent} from './blog-post-card/blog-post-card.component';

const routes: Routes = [
  {
    path: '',
    component: BlogComponent
  },
  {
    path: ':postId',
    component: BlogPostComponent
  }
];

@NgModule({
  imports: [
    CommonModule,
    RouterModule.forChild( routes )
  ],
  exports: [ RouterModule ],
  declarations: [
    // VIEWS
    BlogComponent,
    BlogPostComponent,
    // COMPONENTS
    BlogPostCardComponent
  ]
})
export class BlogModule {}
