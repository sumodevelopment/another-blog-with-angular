import {Component, Input} from '@angular/core';
import {BlogPost} from '../../models/blog-post.model';
import {BlogPostUser} from '../../models/blog-post-user.model';

@Component({
  selector: 'app-blog-post',
  templateUrl: './blog-post.component.html'
})
export class BlogPostComponent {

  public blogPost: BlogPost = {id: -1};
  public blogUser: BlogPostUser = {id: -1};

  constructor() {
  }

}
